function getDate() {
    // Create new timestamp
    var d = new Date();
    var dd = String(d.getDate());
    var mm = String(d.getMonth() + 1)
    var yyyy = String(d.getFullYear());

    // Return in correct formatting
    return dd + '/' + mm + '/' + yyyy;
}

function getFormResults() {
    var sh = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
    var lrow = sh.getLastRow();

    var singleDay = sh.getRange(lrow, 2).getValue()
    var startDate = sh.getRange(lrow, 3).getDisplayValue()
    var endDate = sh.getRange(lrow, 4).getDisplayValue()
    var reason = sh.getRange(lrow, 5).getValue()

    return [singleDay, startDate, endDate, reason]
}


function sendMail(docURL, plain) {
    // Specify message
    var recipent = EMAIL_RECIPENT
    var subject = "Krankmeldung vom " + plain
    var message = "Die folgende Krankmeldung ist zum ausdrucken bereit:\n\n" + docURL

    // Submit email
    MailApp.sendEmail(recipent, subject, message);
}


